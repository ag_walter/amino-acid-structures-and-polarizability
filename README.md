# amino-acids

Structures and 
additional GPAW and ASE input files to calculate the polarizability of amino acids and other small organic molecules in a solvent.
Provided by Katya Zossimova.
